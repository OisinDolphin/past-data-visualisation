<?php

  require_once('config.php');

  $query = "SELECT  
    WEEK(dropoff_time) week,
    COUNT(dropoff_station_id) dropoffs, 
    dropoff_station_id
    FROM past_bookings 
    GROUP BY dropoff_station_id,
    WEEK(dropoff_time)";

  $result = $connection->query($query);

  $objectArray = [];

  class drops {
    function drops($p1, $p2){
      $this->cars = $p1;
      $this->stationId = $p2;
    }
  }

  while($row = mysqli_fetch_assoc($result)){
    $objectArray[$row['week']][] 
    = new drops($row['dropoffs'],$row['dropoff_station_id']);
  }

  header('Content-Type: application/json');

  echo json_encode($objectArray);
?>