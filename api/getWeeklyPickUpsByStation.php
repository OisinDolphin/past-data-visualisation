<?php

  require_once('config.php');

  $query = "SELECT  
    WEEK(pickup_time) week,
    COUNT(pickup_station_id) pickups, 
    pickup_station_id
    FROM past_bookings 
    GROUP BY pickup_station_id,
    WEEK(pickup_time)";

  $result = $connection->query($query);

  $objectArray = [];

  class pickups {
    function pickups($p1,$p2){
      $this->cars = $p1;
      $this->stationId = $p2;
    }
  }

  while($row = mysqli_fetch_assoc($result)){
    $objectArray[$row['week']][] 
    = new pickups($row['pickups'],$row['pickup_station_id']);
  }

  header('Content-Type: application/json');

  echo json_encode($objectArray);
?>