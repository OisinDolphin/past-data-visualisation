<?php

  require_once('config.php');

  $query = "SELECT ANY_VALUE(HOUR(pickup_time)) hour, 
    ANY_VALUE(WEEK(pickup_time)) week,
    COUNT(pickup_station_id) pickups, 
    pickup_station_id, 
    ANY_VALUE(WEEKDAY(pickup_time)) weekDay 
    FROM past_bookings 
    GROUP BY pickup_station_id,DATE(pickup_time),HOUR(pickup_time)";

  $result = $connection->query($query);

  $objectArray = [];

  class stationHour {
    function stationHour($p1,$p2,$p3){
      $this->hour = $p1;
      $this->cars = $p2;
      $this->weekDay = $p3;
    }
  }

  while($row = mysqli_fetch_assoc($result)){

    $objectArray[$row['pickup_station_id']][$row['week']][] 
    = new stationHour($row['hour'],$row['pickups'],$row['weekDay']);
  }

  header('Content-Type: application/json');

  echo json_encode($objectArray);
?>