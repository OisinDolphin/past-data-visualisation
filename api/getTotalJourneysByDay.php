<?php

	require_once('config.php');

	$query = "SELECT DATE(dropoff_time) Date, COUNT(pickup_station_id) Journeys FROM past_bookings GROUP BY Date";
	$result = $connection->query($query);

	$objectArray = [];

	class JourneyDate {
		function JourneyDate($pDate,$pJourneys){
			$this->Date = $pDate;
			$this->Journeys = $pJourneys;
		}
	}

	while($row = mysqli_fetch_assoc($result)){

		$objectArray[] = new JourneyDate($row['Date'],$row['Journeys']);
	}
	
	header('Content-Type: application/json');

	echo json_encode($objectArray);


?>