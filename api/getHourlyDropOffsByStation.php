<?php

  require_once('config.php');

  $query = "SELECT ANY_VALUE(HOUR(dropoff_time)) hour, 
    ANY_VALUE(WEEK(dropoff_time)) week,
    COUNT(dropoff_station_id) dropoffs, 
    dropoff_station_id, 
    ANY_VALUE(WEEKDAY(dropoff_time)) weekDay 
    FROM past_bookings 
    GROUP BY dropoff_station_id,DATE(dropoff_time),HOUR(dropoff_time)";

  $result = $connection->query($query);

  $objectArray = [];

  class stationHour {
    function stationHour($p1,$p2,$p3){
      $this->hour = $p1;
      $this->cars = $p2;
      $this->weekDay = $p3;
    }
  }

  while($row = mysqli_fetch_assoc($result)){

    $objectArray[$row['dropoff_station_id']][$row['week']][] 
    = new stationHour($row['hour'],$row['dropoffs'],$row['weekDay']);
  }

  header('Content-Type: application/json');

  echo json_encode($objectArray);
?>

