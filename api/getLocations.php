<?php
  require_once('config.php');
  /*  This file outputs the longitude and latiude and Id 
      of each station as a json object */
  $query = "SELECT id, longitude, latitude FROM `admin_station`";
  $result = $connection->query($query);
  $objectArray = [];

  class locations {
    function locations($pId,$pLongitude,$pLatitude){
      $this->id = $pId;
      $this->longitude = $pLongitude;
      $this->latitude = $pLatitude;
    }
  }

  while($row = mysqli_fetch_assoc($result)){
    $objectArray[] = new locations($row['id'],$row['longitude'],$row['latitude']);
  }
  
  header('Content-Type: application/json');
  echo json_encode($objectArray);
?>