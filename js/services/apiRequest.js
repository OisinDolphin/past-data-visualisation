(function(){
  angular.module('visualisation')
  .factory('apiRequest',
    function apiRequestFactory($http){
      return {
        journeys: function(){
          return $http({method: "GET", url: "api/getTotalJourneysByDay.php"});
        },
        weeklyDropOffs: function(){
          return $http({method: "GET", url: "api/getWeeklyDropOffsByStation.php"});
        },
        weeklyPickUps: function(){
          return $http({method: "GET", url: "api/getWeeklyPickUpsByStation.php"});
        },
        locations: function(){
          return $http({method: "GET", url: "api/getLocations.php"});
        }
      };
  })
})()