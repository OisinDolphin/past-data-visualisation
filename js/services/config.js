(function(){
  angular.module('visualisation')
    .constant('config',{
      CITY_NAME:"Munich",
      BRUSH_START_DATES: [new Date(2012,01,26), new Date(2012,02,04)],
      STARTING_NODE: 19,
      INITIALISE_ON_WEEKDAYS: true,
      LINECHART_AXIS_MAX: 105,
      MAX_ZOOM: 12,
      MIN_ZOOM: 6,
      START_ZOOM: 11,
      PROJECT_BASE_DIRECTORY: "/project/past-data-visualisation/",
      STARTING_MAP_LOCATION: [48.1345, 11.571]
      //[-33.8650, 151.2094]
    });
})()