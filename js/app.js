(function(){
	angular.module('visualisation',
		['visualisation.barchart',
		'visualisation.map','visualisation.lineChart'])
		.controller('BaseController',[
			'$scope',
			'config',
			function($scope,config){
				$scope.baseDirectory = config.PROJECT_BASE_DIRECTORY;
			}
		])
		//Graph Controller
		//create a constant for the start date and starting selected node
		.controller('MainController', [
			'$scope', 
			'$http',
			'config',
			'apiRequest',
			function($scope, $http, config, apiRequest){

				$scope.pickUps = [];
				$scope.dropOffs = [];
				$scope.brushExtent = config.BRUSH_START_DATES;
				$scope.selectedNode = config.STARTING_NODE;
				$scope.weekDays = config.INITIALISE_ON_WEEKDAYS;
				$scope.lineChartMax = config.LINECHART_AXIS_MAX;
				$scope.minMaxZoom = [config.MIN_ZOOM,config.MAX_ZOOM];
				$scope.startZoom = config.START_ZOOM;
				$scope.startingLocation = config.STARTING_MAP_LOCATION;
				$scope.cityName = config.CITY_NAME;
				$scope.getWeekNumber  = function(date) {
					var firstJan = new Date(date.getFullYear(), 0, 1);
					var milisecondsInDay = 86400000;
					var timeThisYear = ((date - firstJan) / 86400000);

			    return Math.ceil((timeThisYear + firstJan.getDay() + 1) / 7);
				};

				apiRequest.locations()
					.success(function(data) {
						$scope.locations = data;
					});
				$http.get('api/getHourlyPickUpsByStation.php')
					.success(function(data){
						$scope.pickUps = data;
					});
				$http.get('api/getHourlyDropOffsByStation.php')
					.success(function(data){
						$scope.dropOffs = data;
					});
				apiRequest.weeklyDropOffs()
					.success(function(data){
						$scope.weeklyDropOffs = data;
				});
				apiRequest.weeklyPickUps()
					.success(function(data){
						$scope.weeklyPickUps = data;
				});

				apiRequest.journeys()
				.success(function(data){
					$scope.journeys = data;
				});

					
			}
		]);



})()


