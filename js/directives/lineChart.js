(function(){
  angular.module('visualisation.lineChart',[])
    .directive('lineChart',[function(){
      /*
        These variables are outside the main function initialize as they are
        re-used in the draw function
      */
      var width,
      height,
      margin,
      canvas,
      x,
      y,
      xAxis,
      yAxis;
      /*
        The initialize function draws everything on the line chart that does
        not need to be redrawn when the view is updated
      */
      function initializeChart(scope){

        width = $("#lineChart_id").width();
        height = $('#lineChart_id').height();
        margin = {top: 20, right: 20, bottom: 20, left: 40}

        canvas = d3.select("#lineChart_id").append("svg")
           .attr("width",width)
           .attr("height",height)
           .append("g")
           .attr("transform","translate(" + margin.left +"," + margin.top +")");

        width -= margin.right + margin.left;
        height -= margin.top + margin.bottom;
        y = d3.scale.linear()
          .range([height, 0])
          .domain([0, scope.lineChartMax]);

        x = d3.scale.linear()
          .range([0,width])
          .domain([0,24]);

        yAxis = d3.svg.axis()
          .scale(y)
          .orient("left");

        xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom")
          .tickFormat(create24hrFormat);

        function create24hrFormat(d){
          return d>10? d.toString() + ":00":"0" + d.toString() + ":00";
        };


        canvas.append("g")
          .attr("class", "y axis")
          .call(yAxis);

        canvas.append("g")
          .attr("transform","translate(0, " + height + ")")
          .attr("class", "x axis")
          .call(xAxis);

        canvas.append("g")
              .attr("class", "chart dropOffsArea");

        canvas.append("g")
          .attr("class", "chart pickUpsArea");

        canvas.append("g")
          .attr("class", "chart dropOffLine");

        canvas.append("g")
          .attr("class", "chart pickUpLine");


        var LEGEND_MARGIN = 90;
        var SQUARE_SIZE = 15;
        var PADDING = 10;
        var legendNames = ["Drop Offs","Pick Ups"];

        var lineLegend = canvas.append("g")
          .selectAll(".lineLegend")
          .data(legendNames)
          .enter()
          .append("g")
            .attr("class",function(d,i){return i%2==0 ? "overSupplied":"underSupplied";})
            .attr("transform","translate(" + (width - LEGEND_MARGIN) + "," + 0 + ")");

          lineLegend.append("rect")
            .attr("y", function(d,i){return i * (PADDING + SQUARE_SIZE) ;})
            .attr("x", 0)
            .attr("width", SQUARE_SIZE)
            .attr("height", 3);

          lineLegend.append("text")
            .attr("y", function(d,i){return i * (PADDING + SQUARE_SIZE);})
            .attr("x",50)
            .text(function(d){return d;})
            .attr("text-anchor","middle")
            .attr("dy",".35em");

      }
      /*
        Draws Chart lines 
      */
      function drawChart(scope){
        var dropOffData = [];
        var pickUpData = [];
        var startWeek = d3.time.weekOfYear(scope.brushExtent[0]);
        var endWeek = d3.time.weekOfYear(scope.brushExtent[1]);
        if(typeof scope.dropOffs[scope.selectedNode + 1] !== 'undefined'){
          dropOffData = getHourlyAverage(scope.dropOffs[scope.selectedNode + 1],startWeek + 1,endWeek + 1,scope.weekDays);
          dropOffData.push(dropOffData[0]);
        }
        if(typeof scope.pickUps[scope.selectedNode + 1] !== 'undefined'){
          pickUpData = getHourlyAverage(scope.pickUps[scope.selectedNode + 1],startWeek + 1,endWeek + 1,scope.weekDays);
          pickUpData.push(pickUpData[0]);
        }
        //repeat the last axis

        var area = d3.svg.area()
          .interpolate("monotone")
          .x(function(d,i){return x(i)})
          .y0(height)
          .y1(function(d){return y(d)});

        var lineFunction = d3.svg.line()
                                  .x(function(d,i) {return x(i); })
                                  .y(function(d) {return y(d); })
                                  .interpolate("monotone");

        //Transitions from new data to old using the d3 transition function
        //in the linechart
        function transitionToNewData(newData,selecter,pathFunction){
          //Sometimes there is no data for small zones, this if statement
          //ensures a smooth transition to zero in this case by 
          //populating the array with zeros
          if (jQuery.isEmptyObject(newData)){
            newData= new Uint8Array(24);
          }
          //Create linegraph group and assign data
          var linegraph = canvas.selectAll(".chart").filter(selecter)
            .selectAll("path")
            .data([newData]);
          linegraph.enter().append("path");

          linegraph.transition()
          .ease("bounce") //use bounce instead of default linear ease
          .duration(500) //transition to new path takes 500 miliseconds
          .attr("d", pathFunction);
        }
        //apply transition function to 
        transitionToNewData(dropOffData,".dropOffsArea",area);
        transitionToNewData(pickUpData,".pickUpsArea",area);
        transitionToNewData(dropOffData,".dropOffLine",lineFunction);
        transitionToNewData(pickUpData,".pickUpLine",lineFunction);
      }
      //make the averaging function a service
      function getHourlyAverage(data, startWeek, endWeek, weekDays){
        //I understand that this is duplicated logic but I can't see a way to do this without 
        //sacraficing performance
        var returnArray=[];
        for (var i = 0; i < 24; i++) {
          returnArray[i] = 0
        }
        if(weekDays){
          for (var j = startWeek; j < endWeek; j++){
            if(typeof data[j] !== 'undefined'){
              for (var i = 0; i < data[j].length; i++) {
                if(parseInt(data[j][i].weekDay) < 5){ //for weekdays only
                  returnArray[parseInt(data[j][i].hour)] += parseInt(data[j][i].cars); 
                }
              }
            }
          }
        }else{
          for (var j = startWeek; j < endWeek; j++){
            if(typeof data[j] !== 'undefined'){
              for (var i = 0; i < data[j].length; i++) {
                if(parseInt(data[j][i].weekDay) > 4){ //for weekends only
                  returnArray[parseInt(data[j][i].hour)] += parseInt(data[j][i].cars); 
                }
              }
            }
          }
        }

        for (var i = 0; i < 24; i++) {
          if(weekDays){
            returnArray[i] = returnArray[i] / ((endWeek-startWeek) * (5/7));  
          }else{
            returnArray[i] = returnArray[i] / ((endWeek-startWeek) * (2/7));
          }
          
        }
        
        return returnArray;
      }

      var linkFunction = function(scope){ 
        initializeChart(scope);
        //You might run into trouble between pick ups and drop offs but you could just assign them to an array in scope
        function drawOrWait(){
          if(!jQuery.isEmptyObject(scope.dropOffs) && !jQuery.isEmptyObject(scope.pickUps)){
            drawChart(scope);
          }else{
            setTimeout(drawOrWait,500)
          }

        }
        scope.$watch('brushExtent', function() {
          drawOrWait();
        });
        scope.$watch('selectedNode', function(){
          drawOrWait();
        });
        scope.$watch('weekDays', function(){
          drawOrWait();
        });

      };

      return {
        restrict: 'E',
        templateUrl:'js/directives/lineChart.html',
        link: linkFunction
      };

    }])
    
})()