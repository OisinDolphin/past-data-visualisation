(function(){
  angular.module('visualisation.map',[])
    .directive('map',[function(){

      var selectedNode=null;
      var numberOfNodes = 0;
      var cutOff = 0.02
      function drawMap(locationData,scope){
        numberOfNodes = locationData.length;
        var map = new google.maps.Map(d3.select("#map").node(), {
          zoom: scope.startZoom,
          maxZoom: scope.minMaxZoom[1],
          minZoom: scope.minMaxZoom[0],
          center: new google.maps.LatLng(scope.startingLocation[0],scope.startingLocation[1]),
          streetViewControl: false,
          mapTypeControl: false,
          panControl: false,
          draggable: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [{
              stylers: [{
                saturation: -70
              }]
            }]
        });
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(
          document.getElementById('legend'));

        var overlay = new google.maps.OverlayView();

        overlay.onAdd = function() {
            var layer = d3.select(this.getPanes().overlayMouseTarget).append("div")
                .attr("class", "stations");

            // Draw each marker as a separate SVG element.
                // We could use a single SVG, but what size would it have?
            overlay.draw = function() {
            var projection = this.getProjection(),
            padding = 11;

            var marker = layer.selectAll("svg")
                .data(locationData)
                .each(transform) // update existing markers
              .enter().append("svg")
                .each(transform)
                .attr("class", "marker");
            // Add a circle.
            marker.append("circle")
                .attr("r", 9)
                .attr("cx", padding)
                .attr("cy", padding)
                .attr("class","unselected")
                .on("click",function(d,i){switchNodes(i,d3.select(this));})
                .on("mouseover",function(d,i){tempNode(i);})
                .on("mouseout",function(d,i){undoTempNode();});

            if(selectedNode===null){
              var first = d3.selectAll("circle")
                .filter(function(d,i){return i==scope.selectedNode;});
              switchNodes(scope.selectedNode,first);
            }
            styleNodes(scope.weeklyPickUps,scope.weeklyDropOffs,scope.brushExtent[0],scope.brushExtent[1]);
            function transform(d) {
                    d = new google.maps.LatLng(d.latitude, d.longitude);
                    d = projection.fromLatLngToDivPixel(d);
                    return d3.select(this)
                        .style("left", (d.x - padding) + "px")
                        .style("top", (d.y - padding) + "px");
            }
            function switchNodes(index,node){
              if(selectedNode !== null){
                selectedNode.style("stroke","none");
              }
              node.style("stroke","black");
              selectedNode = node;
              selectedNode.index = index;
              scope.$apply(function(){
                scope.selectedNode = index;
              });
            }
            function tempNode(index){
              scope.$apply(function(){
                scope.selectedNode = index;
              });
            }
            function undoTempNode(){
              scope.$apply(function(){
                scope.selectedNode = selectedNode.index;
              });
            }
          };

        };

          overlay.setMap(map);          
      }

    function styleNodes(pickups,dropOffs,brushStartWeek,brushEndWeek){
      var colors = calculateColor(pickups,dropOffs,brushStartWeek,brushEndWeek);
      var opacities = calculateOpacity(pickups,dropOffs,brushStartWeek,brushEndWeek);
      d3.selectAll("circle")
      .transition().duration(500)
      .ease("linear")
      .style("fill",function(d,i){
        return colors[i];
      })
      .style("fill-opacity",function(d,i){
        return opacities[i];
      });
    }
    function calculateColor(pickups,dropOffs,brushStartWeek,brushEndWeek){
      var pickupArray = filterWeeks(pickups,brushStartWeek,brushEndWeek);
      var dropOffArray = filterWeeks(dropOffs,brushStartWeek,brushEndWeek);
      var startWeek = d3.time.weekOfYear(brushStartWeek) + 1;
      var endWeek = d3.time.weekOfYear(brushEndWeek) + 1;
      var number;
      var colArray = [];
      for (var i = 0; i < numberOfNodes; i++) {

        if(typeof dropOffArray[i] === 'undefined' && typeof dropOffArray[i] === 'undefined'){
          number = 0;
        }
        if(typeof dropOffArray[i] === 'undefined' && typeof pickupArray[i] !== 'undefined' ){
          number = 1;
        }
        if(typeof pickupArray[i] === 'undefined' && typeof dropOffArray[i] !== 'undefined'){
          number = -1;
        }
        if(typeof pickupArray[i] !== 'undefined' && typeof dropOffArray[i] !== 'undefined'){
          if( pickupArray[i] == 0 && dropOffArray[i] == 0){
            number = 0;
          }else{
            number = (pickupArray[i] - dropOffArray[i]) / (pickupArray[i] + dropOffArray[i]);
          }
        }
        if(number < (-cutOff)){
          colArray[i] = "green";
        }
        if(number> cutOff){
          colArray[i] = "red";
        }
        if(number <= cutOff && number >= (-cutOff)){
          colArray[i] = "orange";
        }
      };
      return colArray;
    }
    function calculateOpacity(pickups,dropOffs,brushStartWeek,brushEndWeek){
      var pickupArray = filterWeeks(pickups,brushStartWeek,brushEndWeek);
      var dropOffArray = filterWeeks(dropOffs,brushStartWeek,brushEndWeek);
      var startWeek = d3.time.weekOfYear(brushStartWeek) + 1;
      var endWeek = d3.time.weekOfYear(brushEndWeek) + 1;

      var opacityArray = [];
      var max = 0;

      for (var i = 0; i < numberOfNodes; i++) {
        if(max < (pickupArray[i] + dropOffArray[i])){
          max = pickupArray[i] + dropOffArray[i];
        }
      }
      for (var i = 0; i < pickupArray.length; i++) {
        opacityArray[i] = ((pickupArray[i] + dropOffArray[i]) / max) /0.75 + 0.25;
      };
      return opacityArray;
    }
    //make the filter functions services
    function filterWeeks(data,brushStartWeek,brushEndWeek){
      var startWeek = d3.time.weekOfYear(brushStartWeek) + 1;
      var endWeek = d3.time.weekOfYear(brushEndWeek) + 1;
      var returnArray = [];

      for (var i = 0; i < numberOfNodes; i++) {
        returnArray[i] = 0;
      };
      for (var j = startWeek; j < endWeek; j++) {
        for (var i = 0; i < numberOfNodes; i++) {
          if(typeof data[j] !== 'undefined'){
            if(typeof data[j][i] !== 'undefined') {
              returnArray[data[j][i].stationId - 1] += parseInt(data[j][i].cars);
            }
          }

        };
      };
      return returnArray;
    }

    var linkFunction = function(scope){
      function drawOrWait(){
        if(!jQuery.isEmptyObject(scope.weeklyDropOffs) 
          && !jQuery.isEmptyObject(scope.weeklyPickUps) 
          && !jQuery.isEmptyObject(scope.locations)){
          drawMap(scope.locations,scope);
        }else{
          setTimeout(drawOrWait,500);
        }
      }

      drawOrWait();
      scope.$watch('brushExtent', function(){
        if(!jQuery.isEmptyObject(scope.weeklyDropOffs) && !jQuery.isEmptyObject(scope.weeklyPickUps)){
            styleNodes(scope.weeklyPickUps,scope.weeklyDropOffs,scope.brushExtent[0],scope.brushExtent[1]);
        }
      });      
    };




      return {
        restrict: 'E',
        templateUrl:'js/directives/map.html',
        link: linkFunction
      };

    }])
    
})()