(function(){
	angular.module('visualisation.barchart',[])
		.directive('barchart',[function(){

			function drawBarchart(scope){

				var margin = {top:10, right: 30, bottom: 20, left: 20},
					width = $("#barchart_id").width() - margin.left - margin.right,
					height = 120 - margin.top - margin.bottom;

				var length = Object.keys(scope.journeys).length;
				var barWidth = width / length;

				var svg = d3.select("#barchart_id")
					.append("svg")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.append('g')
						.attr("transform","translate(" + margin.left + "," + margin.top +")");

				var yMax = Math.max.apply(Math,scope.journeys.map(function(o){return o.Journeys;}));
				var y = d3.scale.linear()
					.range([height, 0])
					.domain([0, yMax]);

					console.log();

				var x = d3.time.scale()
					.range([0,width])
					.domain(d3.extent(scope.journeys, function(d){ return new Date(d.Date);}));

				var xDomain = d3.extent(scope.journeys, function(d){ return new Date(d.Date);});

				var yAxis = d3.svg.axis()
					.scale(y)
					.orient("left");

				var xAxis = d3.svg.axis()
					.scale(x)
					.orient('bottom')
					.ticks(d3.time.months)
					.tickFormat(d3.time.format("%B"));;

				var startExtent = scope.brushExtent;
				var brush = d3.svg.brush()
					.x(x)
					.extent(startExtent)
					.on("brushend", brushended)
					.on("brushstart", brushstart)
					.on("brush", brushmove);

				context = svg.append("g")
				    .attr("class", "context")
				    .attr("transform", "translate("+ margin.left + ",0)");
 
				context.append("g")
					.attr("class", "y axis")
					.call(yAxis);

				bars = context.selectAll(".bar")
					.data(scope.journeys)
				.enter().append("rect")
					.attr("class", "bar")
					.attr("x", function(d,i){return x(new Date(d.Date));})
					.attr("y", function(d){ return y(d.Journeys);})
					.attr("width", barWidth)
					.attr("height", function(d){ return y(0) - y(d.Journeys)});

				context.append("g")
					.attr('class',"x axis")
					.attr("transform", 'translate(0, ' + height + ')')
					.call(xAxis);

				var brushg = context.append("g")
				    .attr("class", "x brush")
				    .call(brush)

				  brushg.selectAll("rect")
				    .attr("y", -6)
				    .attr("height", height)
				    .attr("transform","translate(0," + 5 + ")");

				brushg.selectAll(".resize").append("rect")
						.attr("height",height)
						.attr("width", 10)
						.attr("rx",5)
						.attr("ry",5)
				    .attr("transform", function(d,i){return i?"translate("+(-10)+",0)":"translate(0,0)";});



				brushstart();
				brushmove();

				function brushstart() {
				  svg.classed("selecting", true);
				}

				function brushmove() {
				  var s = brush.extent();
				  bars.classed("selected", 
				  	function(d) { 
				  		return s[0] <= (new Date(d.Date)) && (new Date(d.Date)) < s[1]; 
				  	});
				}

				function brushended(){

					if (!d3.event.sourceEvent) return; // only transition after input
					var extent0 = brush.extent(), //original brush extent
					    extent1 = extent0.map(d3.time.week.round); //brush extent rounded to a week

					// if empty when rounded, use floor & ceil instead
					if (extent1[0] >= extent1[1]) {
					  extent1[0] = d3.time.week.floor(extent0[0]);
					  extent1[1] = d3.time.week.ceil(extent0[1]);
					}

					scope.$apply(function(){
						scope.brushExtent = extent1;
					})
					svg.classed("selecting", !d3.event.target.empty());

					d3.select(this).transition()
							.duration(250) //use a brush extention time of 250 miliseconds
					    .ease("quad")
					    .call(brush.extent(extent1))
					    .call(brush.event);
				}
			}
			var linkFunction = function(scope){				
				scope.$watch('journeys', function() {
				  if(!jQuery.isEmptyObject(scope.journeys)){
				  	drawBarchart(scope);
				  }
				});			
			};

			return {
				restrict: 'E',
				templateUrl:'js/directives/barchart.html',
				link: linkFunction
			};

		}])
		
})()